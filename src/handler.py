import json

ACCOUNTS = {"trading": 20000, "spend": 42, "saving": 12000, "checking": 20000}


def bls(event, context):
    """
    Main function/entry point routes based on determined state
    """
    state_routing_map = {
        "get_balance": get_balance,
        "funds_transfer": funds_transfer,
        "prioritize_multi_intent": prioritize_multi_intent,
    }

    message = json.loads(event.get("body"))
    state = message.get("classifier_state", message.get("state"))

    handler = state_routing_map.get(state)
    if handler:
        response = handler(message)
    else:
        response = message
    return {
        "statusCode": 200,
        "headers": {"Content-Type": "application/json"},
        "body": json.dumps(response, default=str),
    }


def get_balance(response):
    """
    Update balance slot to include the balance for the requested account
    """
    response = confirm_slots(response)
    response["slots"]["_BALANCE_"] = {
        "type": "int",
        "values": [
            {
                "status": "CONFIRMED",
                "value": ACCOUNTS.get(
                    response["slots"]["_ACCOUNT_NAME_"]["values"][0]["tokens"]
                ),
            }
        ],
    }
    return response


def funds_transfer(response):
    """
    If an account has enough balance make a transaction, confirming account numbers mapped via sre automatically
    """
    response = confirm_slots(response)

    if response.get("slot_relations"):
        sr = response["slot_relations"]

        # Check if a transaction will overdraw an account and label the overdraw slot accordingly
        try:
            transfer_amount = int(response["slots"]["_MONEY_"]["values"][-1]["value"])
            source_account = response["slots"]["_SOURCE_ACCOUNT_"]["values"][0]["value"]
            account_balance = ACCOUNTS.get(source_account)
            if transfer_amount > account_balance:
                overdraw = True
            else:
                overdraw = False

            response["slots"]["_OVERDRAW_"] = {
                "type": "boolean",
                "values": [{"status": "CONFIRMED", "value": overdraw,}],
            }
        except:
            pass

        # Confirm all account nums that have been mapped to an account via sre
        for value in sr:
            account_num = {
                "type": "string",
                "values": [
                    {"value": value["destination"]["value"], "status": "CONFIRMED"}
                ],
            }
            account_num_name = value["source"]["slot"] + "NUM_"
            response["slots"][account_num_name] = account_num

        # Delete all account nums from old slot
        if response["slots"].get("_ACCT_NUM_"):
            for sub_slot in response["slots"]["_ACCT_NUM_"]["values"]:
                sub_slot["status"] = "DELETE"

    return response


def prioritize_multi_intent(response):
    """
    Prioritize multi-intent utterances according to the order they appear in goal_states
    """
    segments = {
        state: utterance for utterance, state in response["classified_segments"]
    }
    goal_states = ["get_balance", "clean_hello", "clean_goodbye", "funds_transfer"]

    ordered = []
    for state in goal_states:
        if segments.get(state):
            ordered.append(segments.get(state))

    if all(ordered):
        response["ordered_segments"] = ordered

    return response


def confirm_slots(response):
    """
    Auto confirm slots giving priority to the last said slot
    """
    for slot in response["slots"].values():
        sub_slot = slot["values"][-1]
        sub_slot["status"] = "CONFIRMED"
        sub_slot["value"] = sub_slot.get("value", sub_slot.get("tokens"))
        slot["values"] = [sub_slot]
    return response
